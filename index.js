function largestPalindromeProduct(minVal, maxVal) {
  let largestPalindrome = 0; 

  for(let i = minVal; i <= maxVal; i++) {
    for(let j = i; j <= maxVal; j++) {
      let product = (i*j) + "";
      let revProduct = product.split("").reduce((revString, char)=> char + revString, "");

      if(revProduct == product) {
        largestPalindrome = product;
      }
    }
  }
  return largestPalindrome;
}

module.exports = largestPalindromeProduct;