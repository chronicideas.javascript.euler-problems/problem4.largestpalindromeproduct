const assert = require('assert');
const largestPalindromeProduct = require('../index.js');

describe('Problem4', function() {
  it('The largest palindrome product from two 2-digit numbers is correct', function() {
    assert.equal(largestPalindromeProduct(10, 99), 9009);
  });
  it('The largest palindrome product from two 3-digit numbers is correct', function() {
    assert.equal(largestPalindromeProduct(100, 999), 888888);
  });
});
